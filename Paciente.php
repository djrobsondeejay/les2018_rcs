<?php
	require_once 'conexao.php';
	class paciente{
		private $id;
        private $nome;
        private $celular;
		private $telefone;
		private $cpf;
        private $endereco;
        private $rg;
        private $comentario;
        private $sms;
        private $email;
        private $nascimento;

		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id=$id;
		}
		public function getNome(){
			return $this->nome;
		}
		public function setNome($nome){
			$this->nome=$nome;
        }
        public function getCelular(){
            return $this->celular;
        }
        public function setCelular($celular){
            $this->celular=$celular; 
        }
		public function getTelefone(){
			return $this->telefone;
		}
		public function setTelefone($telefone){
			$this->telefone=$telefone;
		}                
		public function getCpf(){
			return $this->cpf;
		}
		public function setCpf($cpf){
			$this->cpf=$cpf;
        }
        public function getEndereco(){
			return $this->endereco;
		}
		public function setEndereco($endereco){
			$this->endereco=$endereco;
        }
        public function getRg(){
			return $this->rg;
		}
		public function setRg($rg){
			$this->rg=$rg;
        }
		public function getComentario(){
			return $this->comentario;
		}
		public function setComentario($comentario){
			$this->comentario=$comentario;
		}        
		public function getSms(){
			return $this->sms;
		}
		public function setSms($sms){
			$this->sms=$sms;
		}
		public function getEmail(){
			return $this->email;
		}
		public function setEmail($email){
			$this->email=$email;
        }
		public function getNascimento(){
			return $this->nascimento;
		}
		public function setNascimento($nascimento){
			$this->nascimento=$nascimento;
		}        
    
		public function buscarTodos(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from paciente order by nome"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
                    "insert into paciente(nome,celular,telefone,cpf,endereco,rg,comentario,sms,email,nascimento) 
                    values(:n,:cel,:tel,:cpf,:end,:rg,:com,:sms,:email,:nasc)"
				);
				$stmt->bindValue(":n",$this->getNome());
				$stmt->bindValue(":cel",$this->getCelular());
                $stmt->bindValue(":tel",$this->getTelefone());
                $stmt->bindValue(":cpf",$this->getCpf());
                $stmt->bindValue(":end",$this->getEndereco());
                $stmt->bindValue(":rg",$this->getRg());
                $stmt->bindValue(":com",$this->getComentario());
                $stmt->bindValue(":sms",$this->getSms());
                $stmt->bindValue(":email",$this->getEmail());
                $stmt->bindValue(":nasc",$this->getNascimento());
				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}	




		public function excluir(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"delete from paciente where id=:id "
				);
				$stmt->bindValue("id",$this->getId());
				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function buscarId(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from paciente where id=:id"
				);
				$stmt->bindValue(":id",$this->getId());
				$stmt->execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function alterar(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"update paciente set nome=:n,celular=:cel,telefone=:tel,cpf=:cpf,endereco=:end,rg=:rg,
					comentario=:com,sms=:sms,email=:email,nascimento=:nasc"." where id=:id"
				);
				
				$stmt->bindValue(":n",$this->getNome());
				$stmt->bindValue(":cel",$this->getCelular());
                $stmt->bindValue(":tel",$this->getTelefone());
                $stmt->bindValue(":cpf",$this->getCpf());
                $stmt->bindValue(":end",$this->getEndereco());
                $stmt->bindValue(":rg",$this->getRg());
                $stmt->bindValue(":com",$this->getComentario());
                $stmt->bindValue(":sms",$this->getSms());
                $stmt->bindValue(":email",$this->getEmail());
                $stmt->bindValue(":nasc",$this->getNascimento());


				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>