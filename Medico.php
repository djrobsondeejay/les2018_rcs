<?php
	require_once 'conexao.php';
	class medico{
		private $id;
        private $nome;
        private $especialidade;
        private $crm;
        private $sala;
        private $celular;
		private $telefone;

		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id=$id;
		}
		public function getNome(){
			return $this->nome;
		}
		public function setNome($nome){
			$this->nome=$nome;
        }
        public function getEspecialidade(){
			return $this->especialidade;
		}
		public function setEspecialidade($especialidade){
			$this->especialidade=$especialidade;
        }
		public function getCrm(){
			return $this->crm;
		}
		public function setCrm($crm){
			$this->crm=$crm;
        }
        public function getSala(){
			return $this->sala;
		}
		public function setSala($sala){
			$this->sala=$sala;
        }
        public function getCelular(){
            return $this->celular;
        }
        public function setCelular($celular){
            $this->celular=$celular; 
        }
		public function getTelefone(){
			return $this->telefone;
		}
		public function setTelefone($telefone){
			$this->telefone=$telefone;
		}

		public function buscarTodos(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from medico order by nome"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function buscarNome(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select nome from medico order by nome"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		
		public function inserir(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
                    "insert into medico(nome,especialidade,crm,sala,celular,telefone) 
                    values(:n,:esp,:crm,:sala,:cel,:tel)"
				);
				$stmt->bindValue(":n",$this->getNome());
                $stmt->bindValue(":esp",$this->getEspecialidade());
                $stmt->bindValue(":crm",$this->getCrm());
                $stmt->bindValue(":sala",$this->getSala());
                $stmt->bindValue(":cel",$this->getCelular());
                $stmt->bindValue(":tel",$this->getTelefone());
				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		

		public function excluir(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"delete from medico where id=:id "
				);
				$stmt->bindValue("id",$this->getId());
				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function buscarId(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from medico where id=:id"
				);
				$stmt->bindValue(":id",$this->getId());
				$stmt->execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function alterar(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"update medico set nome=:n,especialidade=:esp,crm=:crm,sala=:sala,celular=:cel,telefone=:tel"." where id=:id"
				);			
				$stmt->bindValue(":n",$this->getNome());
				$stmt->bindValue(":esp",$this->getEspecialidade());
				$stmt->bindValue(":crm",$this->getCrm());
				$stmt->bindValue(":sala",$this->getSala());
				$stmt->bindValue(":cel",$this->getCelular());
				$stmt->bindValue(":tel",$this->getTelefone());


				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>