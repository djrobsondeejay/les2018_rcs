<?php
	require_once 'conexao.php';
	class consulta{
		private $id;
        private $data;
        private $status;
        private $idmed;
        private $idpac;

		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id=$id;
		}
		public function getData(){
			return $this->data;
		}
		public function setData($data){
			$this->data=$data;
        }
        public function getStatus(){
			return $this->status;
		}
		public function setStatus($status){
			$this->status=$status;
        }
		public function getIdmed(){
			return $this->idmed;
		}
		public function setIdmed($idmed){
			$this->idmed=$idmed;
        }
        public function getIdpac(){
			return $this->idpac;
		}
		public function setIdpac($idpac){
			$this->idpac=$idpac;
        }
       
		public function buscarTodos(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from consulta order by data"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		

		public function buscarTudo(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"SELECT c.id, c.data, c.status, m.nome AS nomeMed, p.nome AS nomePac
					FROM consulta as c 
					INNER JOIN medico as m ON m.id = c.idMed 
					INNER JOIN paciente as p ON p.id = c.idpac
					ORDER BY c.data"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
        }

        public function buscarData(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from consulta where convert(char(17),data,113) = convert(char(17),getdate(),113) order by data"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
        }

		public function inserir(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
                    "insert into consulta(data,status,idMed,idPac) 
					values(:d,:s,:idm,:idp)"
				);
				$stmt->bindValue(":d",$this->getData());
                $stmt->bindValue(":s",$this->getStatus());
                $stmt->bindValue(":idm",$this->getIdmed());
                $stmt->bindValue(":idp",$this->getIdpac());


				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		
		public function excluir(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"delete from consulta where id=:id "
				);
				$stmt->bindValue("id",$this->getId());
				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function buscarId(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from consulta where id=:id"
				);
				$stmt->bindValue(":id",$this->getId());
				$stmt->execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function alterar(){
			$c= new conexao();
			try{
				$stmt=$c->conn->prepare(
					"update consulta set data=:d,status=:s,idMed=:idm, idPac=idp"." where id=:id"
				);
				$stmt->bindValue(":d",$this->getData());
				$stmt->bindValue(":s",$this->getStatus());
				$stmt->bindValue(":idm",$this->getIdmed());
				$stmt->bindValue(":idp",$this->getIdpac());
				return  $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}



	}
?>