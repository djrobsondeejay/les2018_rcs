-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Nov-2018 às 20:55
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medconsult`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `consulta`
--

CREATE TABLE `consulta` (
  `id` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `idMed` int(11) NOT NULL,
  `idPac` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `consulta`
--

INSERT INTO `consulta` (`id`, `data`, `status`, `idMed`, `idPac`) VALUES
(1, '2018-11-20 14:20:00', 'Finalizada', 11, 6),
(2, '2018-11-30 10:00:00', 'Aguardando', 12, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `medico`
--

CREATE TABLE `medico` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `especialidade` varchar(100) NOT NULL,
  `crm` varchar(15) NOT NULL,
  `sala` varchar(30) NOT NULL,
  `celular` varchar(14) NOT NULL,
  `telefone` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `medico`
--

INSERT INTO `medico` (`id`, `nome`, `especialidade`, `crm`, `sala`, `celular`, `telefone`) VALUES
(11, 'Carlos Correia', 'Urologista', '223344SP', '10', '1997866788', '5677788888'),
(12, 'Fernando Chaves', 'cardiologista', '987789987SP', '02', '987654333', '4567771'),
(13, 'Karina Bitencurt', 'Pediatra', '33445566SP', '05', '987787656', '4567272727');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paciente`
--

CREATE TABLE `paciente` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `celular` varchar(14) NOT NULL,
  `telefone` varchar(14) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `endereco` varchar(200) DEFAULT NULL,
  `rg` varchar(8) DEFAULT NULL,
  `comentario` varchar(1000) DEFAULT NULL,
  `sms` tinyint(1) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nascimento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `paciente`
--

INSERT INTO `paciente` (`id`, `nome`, `celular`, `telefone`, `cpf`, `endereco`, `rg`, `comentario`, `sms`, `email`, `nascimento`) VALUES
(5, 'Maria de Souza', '1199999999', '45454545', '3455433451', 'Rua Austria, n40, Jd. Nacoes, Itatiba SP', '45555444', 'teste', 1, 'email@email.com', '0000-00-00'),
(6, 'Joao Paulo', '98766543', '44554455', '9877899872', 'Av Nossa Senhora das Gracas, Recanto da Capela, Itatiba SP', '77888999', 'teste', 127, 'joao@joao.com.br', '0000-00-00'),
(7, 'Robson Silva', '98027300', '44874487', '36536536509', 'Rua Humberto Primo, 90, Pq. Sao Francisco, Itatiba SP', '46776776', 'faze de teste', 0, 'robson@robson.com', '0000-00-00'),
(8, 'Ricardo Marques', '977667766', '45677645', '3677789908', 'Av. Vicente Catallani, 1000, Bairro das Brotas, Itatiba SP', '43344333', 'r', 1, 'ricardo@uol.com', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consulta`
--
ALTER TABLE `consulta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consulta`
--
ALTER TABLE `consulta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `medico`
--
ALTER TABLE `medico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
