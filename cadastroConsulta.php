<!DOCTYPE html>
<html lang="pt-br">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="Form Cadasrto consultas" content="formulario de cadastro de consultas">
    <meta name="Autor RobsonCaputo" content="">

    <title>MedConsult</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">



  </head>

  <body id="page-top">

    
    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">

        <li class="nav-item active">
          <a class="nav-link" href="index.php"><img src= "./image/MedConsult.png" width=200px height=180px/></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Agenda</span>
          </a>
        </li>

        <li class="nav-item active">
          <a class="nav-link" href="404.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Agenda por Médico</span>
          </a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Cadastros</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">			
			        <h6 class="dropdown-header">Cadastros:</h6>
                <a class="dropdown-item" href="cadastroPaciente.php">Cadastro de Pacientes</a>
                <a class="dropdown-item" href="cadastroMedico.php">Cadastro de Medicos</a>
                <a class="dropdown-item" href="404.html">Cadastro de Usuarios</a>
            </div>
        
		
		      <li class="nav-item">
            <a class="nav-link" href="cadastroConsulta.php">
              <i class="fas fa-fw fa-table"></i>
              <span>Consultas</span></a>
            </li>
		
		      <li class="nav-item">
            <a class="nav-link" href="404.html">
              <i class="fas fa-fw fa-table"></i>
              <span>Relatorios</span></a>
           </li>
		
		
		
        </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active">Consultas</li>
          </ol>
        

                 <!-- FORMULARIO MEDICO-->        
                  <div class="container">
	
                    <div class="card card-register mx-auto mt-5">
                      <div class="card-header">Cadastro de Consultas</div>
                        <div class="card-body">

                        <?php
                        require_once 'Consulta.php';
                        $c= new consulta();
                            if(isset($_POST['data'])){
                                $c->setData($_POST['data']);
                                $c->setStatus($_POST['status']);
                                $c->setIdmed($_POST['idMed']);
                                $c->setIdpac($_POST['idPac']);
                      
                                $c->inserir();
                                unset($_POST['data']);
                                // header("Refresh:0");
                            }
                        ?>  
                            <form method="POST">

                            <div class="form-group">
                                <label for="data">Data:</label>
                                <div class="form-label-group">
                                <input type="date" name="data" class="form-control" placeholder="Data" required="required" autofocus="true">
                                <label for="data">Data</label>
                                </div>
                            </div>


                            <div class="form-group">
                              <label for="status">Status:</label>
                              <select type="text" name="status" class="form-control" id="status">
                                <option>Aguardando</option>
                                <option>Atrazada</option>
                                <option>Em Andamento</option>
                                <option>Finalizada</option>
                              </select>
                            </div>


                            <div class="form-group">
                              <label for="medico">Medico</label>
                              <select type="text" name="idMed" class="form-control" id="medico">

                                <?php 
                                require_once 'Medico.php';
                                $c= new medico();
                                $consulta=$c->buscarTodos();
                                foreach($consulta as $linha){
                                  print("<option value='".$linha['id']."'>".$linha['nome']."</option>"); 
                                } 
                                ?>
                              </select>
                            </div>


                            <div class="form-group">
                              <label for="paciente">Paciente</label>
                              <select type="text" name="idPac" class="form-control" id="paciente" placeholder="Paciente">

                                <?php 
                                require_once 'Paciente.php';
                                $c= new paciente();
                                $consulta=$c->buscarTodos();
                                foreach($consulta as $linha){
                                  print("<option value='".$linha['id']."'>".$linha['nome']."</option>"); 
                                } 
                                ?>
                              </select>
                            </div >

                            <input class="btn btn-primary" type="submit" value="inserir">
                            <input class="btn btn-secundary"type="Reset" value="limpa">
                            <button class="btn btn-secundary" >Cancelar</button>
 
                            </form>

                            <div class="form-group">
                            <button class="btn btn-secondary"  >Editar/Excluir</button>
                            </div>
                            

                        </div>
                        </div>
                    </div>


        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © MedConsult 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
